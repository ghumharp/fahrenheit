package sheridan;

public class Fahrenheit {
	
	public static int fromCelsius(int celsius ) {
		 double fahrenheit = ((5.0/9) * celsius) + 32;
		int value = (int)Math.round(fahrenheit);
		return value;
	}

}
