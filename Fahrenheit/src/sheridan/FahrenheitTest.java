package sheridan;
/*
 * Harpreet Ghuman
 */

import static org.junit.Assert.*;

import org.junit.Test;

public class FahrenheitTest {

	@Test
	public void testFromCelciusRegular() {
		//fail("Not yet implemented");
		int temp = Fahrenheit.fromCelsius(18);
		assertTrue("Incorrect Temperature", temp == 64 );
	}
	@Test
	public void testFromCelciusException() {
		//fail("Not yet implemented");
		int temp = Fahrenheit.fromCelsius(-1700);
		assertFalse("Incorrect Temperature", temp == 150000 );
		
	}
	@Test
	public void testFromCelciusBoundryIn() {
		//fail("Not yet implemented");
		int temp = Fahrenheit.fromCelsius(1);
		assertTrue("Incorrect Temperature", temp == 33 );
	}
	@Test
	public void testFromCelciusBoundryOut() {
		//fail("Not yet implemented");
		int temp = Fahrenheit.fromCelsius(-1);
		assertFalse("Incorrect Temperature", temp == 150 );
	}

}
